const CircularJSON = require('circular-json');
bodyParser = require('body-parser').json();
var cors = require('cors')

var ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {

app.use(cors())
      
      //1. UPDATE EXISITING JOB POST
      app.put('/update/:id', bodyParser, (req, res) => {
          const id = req.params.id;
          const details = { '_id': new ObjectID(id) };
          const json = CircularJSON.stringify(req.body);
            var options = {
                body : json
            };
          db.collection('Job_Listing').update(details, options, (err, result) => {
              if (err) {
                  res.send({'error':'An error has occurred'});
                  } else {
                      res.send(options);
                      }
            });
        });

        //2. APPLY FOR A RELEVANT JOB POST

        const applyForJob = app.post('/apply',bodyParser, (req, res) => {
            const json = CircularJSON.stringify(req.body);
            var options = {
                body : json
            };
            db.collection('Applications').insertOne(options, (err, result) => {
                if (err) {
                    res.send({ 'error': 'An error has occurred' });
                }
                else {
                    res.send(result.ops[0]);
            }    
            }); 
        });


    //3. DELETE IRRELEVANT OR EXPIRED JOB POSTS
      app.delete('/delete/:id', (req, res) => {
          const id = req.params.id;
          const details = { '_id': new ObjectID(id) };
          db.collection('Job_Listing').remove(details, (err, item) => {
              if (err) {
                  res.send({'error':'An error has occurred'});
                  } else {
                      res.send('Job ' + id + ' deleted!');
                      }     
            });  
        });

    //4. GET PARTICULAR JOB DETAILS

        app.get('/jobs/:id', (req, res) => {
            const id = req.params.id;
            const details = { '_id': new ObjectID(id) };
            db.collection('Job_Listing').findOne(details, (err, item) => {
            if (err) {
                res.send({'error':'An error has occurred'});
            }
            else {
                res.send(item);
            }
            });
        });


    //5. GET A LIST OF ALL OPEN JOBS
    
        app.get('/jobs/', (req, res) => {
            db.collection('Job_Listing').find({}).toArray((err, result) => {
            if (err) {
                res.send({'error':'No documents found'});
            }
            else {
                    res.send(JSON.stringify(result));
            }
            });
        });

    //6. POST A NEW JOB
        const jobListing = app.post('/add',bodyParser, (req, res) => {
            const json = CircularJSON.stringify(req.body);
            var options = {
                body : json
            };
            db.collection('Job_Listing').insertOne(options, (err, result) => {
                if (err) {
                    res.send({ 'error': 'An error has occurred' });
                }
                else {
                    res.send(result.ops[0]);
            }    
            }); 
        });


    //6. GET APPLIED JOB APPLICATIONS
        app.get('/applications', (req, res) => {
            db.collection('Applications').find({}).toArray((err, result) => {
            if (err) {
                res.send({'error':'No documents found'});
            }
            else {
                    res.send(JSON.stringify(result.reverse()));
            }
            });
        });
    };

