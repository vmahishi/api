# README #

### What is this repository for? ###

* Backend apis for job search and application portal

### How do I get set up? ###

* Summary of set up

*    Open a terminal on your system
*    Enter git clone git clone https://vmahishi@bitbucket.org/vmahishi/api.git and hit enter
*   Change your directory to api
*    On the terminal enter 'yarn install' or 'npm install' based on the package manager installed

*    Ta da, your backend server is configured and is ready to serve api requests from the frontend. You don't need to configure the DB, I have used MLab and have integrated with the server.
*   All you have to do is install and start using.
*   For your api documentation, once the server is up and running, enter 'http://localhost:8000/api-docs/' in your browser and your swagger is up and running.
*   You can check out the list of api's and can even play around with it.